package br.com.lens.test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.NamedEvent;

@NamedEvent
@SessionScoped
public class NavigationBean implements Serializable {
  private static final long serialVersionUID = 1L;
 
  private String page="pessoaFisica.xhtml";
 
  public String getPage() {
    return page;
  }
 
  public void setPage(String currentPage) {
    this.page=currentPage;
  }
}
package br.com.lens.bean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
//import org.apache.log4j.Logger;
import br.com.lens.dao.UsuarioDao;
import br.com.lens.model.Usuario;

@ManagedBean
@ViewScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	//private static Logger logger = Logger.getLogger(LoginBean.class);
	
	private Usuario usuario = new Usuario();

	public Usuario getUsuario() {
		return usuario;
	}

	public String efetuaLogin() {
		
		
		 System.out.println("Fazendo login do usuario " + this.usuario.getEmail());
		 
		 FacesContext context = FacesContext.getCurrentInstance(); 
		 boolean existe = new UsuarioDao().existe(this.usuario);
		  
		  if (existe) {
		  
		  context.getExternalContext().getSessionMap() .put("usuarioLogado",
		  this.usuario);
		  
		  return "home?faces-redirect=true"; }
		  
		  context.getExternalContext().getFlash().setKeepMessages(true);
		  context.addMessage("loginForm:password", new FacesMessage("Usuario n�o encontrado"));
		  
		  return "login?faces-redirect=true";
		 
		/*
		String ret = null;
		FacesMessage message = null;
		boolean loggedIn = false;
		FacesContext context = FacesContext.getCurrentInstance();

		if (this.usuario.getEmail() != null && this.usuario.getSenha() != null) {
			loggedIn = true;
			message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bem Vindo", this.usuario.getEmail());
			//ret = "login?faces-redirect=true";
		} else {
			loggedIn = false;
			//message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro Login", "Senha Inválida");
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage("Usuario nao encontrado"));
		}

		context.addMessage(null, message);
		PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
		return ret;
		*/
	}

	public String deslogar() {

		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().remove("usuarioLogado");

		return "login?faces-redirect=true";
	}

}

package br.com.lens.bean;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;


@ManagedBean
@RequestScoped
public class MenuBean implements Serializable {

	private static final long serialVersionUID = 1L;
	// contents of menu
	
	private boolean archiveMenu;
	
	/**
	 * @return the archiveMenu
	 */
	public boolean isArchiveMenu() {
		return archiveMenu;
	}

	@PostConstruct
	public void init() {
		
		// creating menu
		archiveMenu = false;
	
	}
}
package br.com.lens.bean;

import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
//import org.apache.log4j.Logger;
import br.com.lens.dao.DAO;
import br.com.lens.model.ContatoPessoa;
import br.com.lens.model.Endereco;
import br.com.lens.model.PessoaFisica;
import br.com.lens.model.ReferenciaPessoa;


@ManagedBean
@ViewScoped
public class PessoaFisicaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	//private static Logger logger = Logger.getLogger(PessoaFisicaBean.class);

	private PessoaFisica pessoaFisica = new PessoaFisica();

	private Integer pessoaFisicaId;

	private String filter;
	
	private boolean cityFilter;

	/**
	 * @return the pessoaFisicaId=
	 */
	public Integer getPessoaFisicaId() {
		return pessoaFisicaId;
	}

	/**
	 * @param pessoaFisicaId the pessoaFisicaId to set
	 */
	public void setPessoaFisicaId(Integer pessoaFisicaId) {
		this.pessoaFisicaId = pessoaFisicaId;
	}

	public List<PessoaFisica> getPessoaFisicas() {

		if (filter == null) {

			return new DAO<PessoaFisica>(PessoaFisica.class).listaTodos();

		} else {

			System.err.println(">>>FILTER:::"+filter);
			System.err.println(">>>CITY FILTER:::"+isCityFilter());
			return new DAO<PessoaFisica>(PessoaFisica.class).listaTodos(filter, isCityFilter());

		}

	}

	public void search() {

	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}	

	public void remover(PessoaFisica pessoaFisica) {

		System.out.println(">>>>>REMOVENDO PESSOA<<<< " + pessoaFisica.getNome());
		new DAO<PessoaFisica>(PessoaFisica.class).remove(pessoaFisica);
		FacesContext.getCurrentInstance().addMessage("pessoaFisica",
				new FacesMessage(FacesMessage.SEVERITY_INFO, "REGISTRO REMOVIDO COM SUCESSO.","REGISTRO REMOVIDO COM SUCESSO."));
	}

	public String gravar() {
/*
		if (pessoaFisica.getEnderecos().isEmpty()) {

			FacesContext.getCurrentInstance().addMessage("pessoa",
					new FacesMessage("PESSOA DEVE TER PELO MENO UM ENDERE�O!"));

			return null;

		}
*/
		if (pessoaFisica.getContatos().isEmpty()) {

			FacesContext.getCurrentInstance().addMessage("pessoaFisica",
					new FacesMessage(FacesMessage.SEVERITY_WARN, "INSIRA PELO MENOS UM CONTATO","INSIRA PELO MENOS UM CONTATO"));

			return null;

		}

		if (this.pessoaFisica.getId_pessoa_fisica() == null) {

			System.out.println(">>>>>>>>SALVANDO CADASTRO PESSOA<<<<<<<");
			new DAO<PessoaFisica>(PessoaFisica.class).adiciona(this.pessoaFisica);
			FacesContext.getCurrentInstance().addMessage("pessoaFisica",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "PESSOA CADASTRADA COM SUCESSO.","PESSOA CADASTRADA COM SUCESSO."));

		} else {

			System.out.println(">>>>>>>>ATUALIZANDO CADASTRO PESSOA<<<<<<<");
			new DAO<PessoaFisica>(PessoaFisica.class).atualiza(this.pessoaFisica);
			FacesContext.getCurrentInstance().addMessage("pessoaFisica",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "REGISTRO ALTERADO COM SUCESSO.","REGISTRO ALTERADO COM SUCESSO."));

		}

		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		this.pessoaFisica = new PessoaFisica();

		return "listaPessoaFisica?faces-redirect=true";
	}

	public void gravarEndereco(Endereco end) {

		System.out.println(">>>>>>>>OBJ SEND:::" + end);
		for (Endereco iterable : pessoaFisica.getEnderecos()) {

			System.out.println(">>>>>>>>OBJ LIST:::" + iterable);
		}

		if (pessoaFisica.getEnderecos().contains(end)) {

			System.out.println(">>>>>>>>Alterando endere�o " + end.getEndereco());
			pessoaFisica.alterarEndereco(end);
			FacesContext.getCurrentInstance().addMessage("end", new FacesMessage("ENDERE�O ALTERADO COM SUCESSO!"));

		} else {

			System.out.println(">>>>>>>>Adicionando endere�o " + end.getEndereco());
			pessoaFisica.adicionaEndereco(end);
			FacesContext.getCurrentInstance().addMessage("end", new FacesMessage("ENDERE�O ADICIONADO COM SUCESSO!"));
		}
	}

	public void removerEndereco(Endereco end) {

		System.out.println(">>>>>>>>Removendo endere�o " + end.getEndereco());
		pessoaFisica.removeEndereco(end);
		FacesContext.getCurrentInstance().addMessage("pessoa", new FacesMessage("ENDERE�O REMOVIDO COM SUCESSO!"));
	}

	public void gravarContato(ContatoPessoa contatoPessoa) {

		System.out.println(">>>>>>>>Contato " + contatoPessoa);
		
		if (pessoaFisica.getContatos().contains(contatoPessoa)) {

			System.out.println(">>>>>>>>Alterando contato " + contatoPessoa.getValor_contato());
			pessoaFisica.alterarContato(contatoPessoa);			
			FacesContext.getCurrentInstance().addMessage("contatoPessoa",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "CONTATO ALTERADO COM SUCESSO!","CONTATO ALTERADO COM SUCESSO!"));

		} else {

			System.out.println(">>>>>>>>Inserindo tipo contato " + contatoPessoa.getTipo_contato());
			System.out.println(">>>>>>>>Inserindo contato " + contatoPessoa.getValor_contato());
			pessoaFisica.adicionaContato(contatoPessoa);			
			FacesContext.getCurrentInstance().addMessage("contatoPessoa",
					new FacesMessage(FacesMessage.SEVERITY_INFO, "CONTATO INSERIDO COM SUCESSO!","CONTATO INSERIDO COM SUCESSO!"));
		}
	}

	public void removerContato(ContatoPessoa contatoPessoa) {

		System.out.println(">>>>>>>>Removendo contato " + contatoPessoa.getValor_contato());
		pessoaFisica.removeContato(contatoPessoa);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("CONTATO REMOVIDO COM SUCESSO!"));
	}

	public void gravarReferencia(ReferenciaPessoa referenciaPessoa) {

		if (pessoaFisica.getReferencias().contains(referenciaPessoa)) {

			System.out.println(">>>>>>>>Alterando ref�ncia " + referenciaPessoa.getNome_referencia());
			pessoaFisica.alterarReferencia(referenciaPessoa);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("REFER�NCIA ALTERADO COM SUCESSO!"));

		} else {

			System.out.println(">>>>>>>>Adicionando endere�o " + referenciaPessoa.getNome_referencia());
			pessoaFisica.adicionaReferencia(referenciaPessoa);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("REFER�NCIA ADICIONADO COM SUCESSO!"));
		}

	}

	public void removerReferencia(ReferenciaPessoa referenciaPessoa) {

		System.out.println(">>>>>>>>Removendo referencia " + referenciaPessoa.getNome_referencia());
		pessoaFisica.removeReferencia(referenciaPessoa);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("REFER�NCIA REMOVIDA COM SUCESSO!"));
	}

	/**
	 * @return the pessoaFisica
	 */
	public PessoaFisica getPessoaFisica() {

		return pessoaFisica;
	}

	/**
	 * @param pessoaFisica the pessoaFisica to set
	 */
	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public void novoCadastroPessoa() {

		System.out.println(">>>>>>>>NOVO CADASTRO PESSOA<<<<<<<");
		this.pessoaFisica = new PessoaFisica();
		// return "pessoaFisica?faces-redirect=true";
	}

	public String detalhar(PessoaFisica pessoaFisica) {

		System.out.println(">>>>>>>>DETALHANDO PESSOA<<<<<<<");
		return "pessoaFisica?faces-redirect=true&pessoaFisicaId=" + pessoaFisica.getId_pessoa_fisica();
	}

	public void carregarPessoaFisicaPelaId() {

		System.out.println(">>>>>>>>PEGOU PESSOA PELA ID:::: " + pessoaFisicaId);
		this.pessoaFisica = new DAO<PessoaFisica>(PessoaFisica.class).buscaPorId(pessoaFisicaId);

		for (Endereco iterable : this.pessoaFisica.getEnderecos()) {

			System.out.println(">>>>>>>>ENDERCO PESSOA:::: " + iterable.getEndereco());
		}
	}

	public boolean isCityFilter() {
		return cityFilter;
	}

	public void setCityFilter(boolean cityFilter) {
		this.cityFilter = cityFilter;
	}

}

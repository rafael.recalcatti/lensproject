package br.com.lens.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import br.com.lens.model.Endereco;

@ManagedBean
@ViewScoped
public class EnderecoBean {

	private Endereco endereco = new Endereco();
	
	private static Logger logger = Logger.getLogger(EnderecoBean.class);

	/**
	 * @return the endereco
	 */
	public Endereco getEndereco() {
		return endereco;
	}

	public void carregar(Endereco endereco) {
		System.out.println("Carregando endereco");
		System.out.println(">>>>>>>>OBJ NO CARREGAMENTO::::::" + endereco);
		System.out.println(">>>>>>>>ID NO CARREGAMENTO::::::" + endereco.getId_endereco());
		this.endereco = endereco;
	}

	public void limpar() {

		this.endereco = new Endereco();
	}

}

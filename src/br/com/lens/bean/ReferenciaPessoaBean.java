package br.com.lens.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import org.apache.log4j.Logger;
import br.com.lens.model.ReferenciaPessoa;

@ManagedBean
@ViewScoped
public class ReferenciaPessoaBean {

	private ReferenciaPessoa referenciaPessoa = new ReferenciaPessoa();
	
	//private static Logger logger = Logger.getLogger(ReferenciaBean.class);
	
	/**
	 * @return the contato
	 */
	public ReferenciaPessoa getReferenciaPessoa() {
		return referenciaPessoa;
	}

	public void carregar(ReferenciaPessoa referenciaPessoa) {
		System.out.println("Carregando reeferencia");
		this.referenciaPessoa = referenciaPessoa;
	}

	public void limpar() {

		this.referenciaPessoa = new ReferenciaPessoa();
	}
}

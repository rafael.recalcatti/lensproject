package br.com.lens.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import org.apache.log4j.Logger;
import br.com.lens.model.ContatoPessoa;

@ManagedBean
@ViewScoped
public class ContatoPessoaBean {

	private ContatoPessoa contatoPessoa = new ContatoPessoa();
	// private static Logger logger = Logger.getLogger(ContatoBean.class);
	
	/**
	 * @return the contatoPessoa
	 */
	public ContatoPessoa getContatoPessoa() {
		return contatoPessoa;
	}

	public void carregar(ContatoPessoa contatoPessoa) {
		System.out.println("Carregando contato");
		this.contatoPessoa = contatoPessoa;
	}

	public void limpar() {

		this.contatoPessoa = new ContatoPessoa();
	}

}

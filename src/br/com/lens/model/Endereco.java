package br.com.lens.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "endereco")
public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id_endereco;
	private String tipo_endereco;
	private String logradouro;
	private String endereco;
	private Integer numero;
	private String cep;
	private String bairro;
	private String cidade;
	private String uf;
	private String std_endereco;
	
	@ManyToOne
	@JoinColumn(name = "id_pessoa_fisica", insertable=false, updatable=false)
	private PessoaFisica pessoaFisica;
	/**
	 * @return the pessoaFisica
	 */
	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}
	/**
	 * @param pessoaFisica the pessoaFisica to set
	 */
	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
	/**
	 * @return the id_endereco
	 */
	public Integer getId_endereco() {
		return id_endereco;
	}
	/**
	 * @param id_endereco the id_endereco to set
	 */
	public void setId_endereco(Integer id_endereco) {
		this.id_endereco = id_endereco;
	}
	/**
	 * @return the tipo_endereco
	 */
	public String getTipo_endereco() {
		return tipo_endereco;
	}
	/**
	 * @param tipo_endereco the tipo_endereco to set
	 */
	public void setTipo_endereco(String tipo_endereco) {
		this.tipo_endereco = tipo_endereco;
	}
	/**
	 * @return the logradouro
	 */
	public String getLogradouro() {
		return logradouro;
	}
	/**
	 * @param logradouro the logradouro to set
	 */
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	/**
	 * @return the endereco
	 */
	public String getEndereco() {
		return endereco;
	}
	/**
	 * @param endereco the endereco to set
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	/**
	 * @return the numero
	 */
	public Integer getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	/**
	 * @return the cep
	 */
	public String getCep() {
		return cep;
	}
	/**
	 * @param cep the cep to set
	 */
	public void setCep(String cep) {
		this.cep = cep;
	}
	/**
	 * @return the bairro
	 */
	public String getBairro() {
		return bairro;
	}
	/**
	 * @param bairro the bairro to set
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	/**
	 * @return the cidade
	 */
	public String getCidade() {
		return cidade;
	}
	/**
	 * @param cidade the cidade to set
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}
	/**
	 * @param uf the uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}
	/**
	 * @return the std_endereco
	 */
	public String getStd_endereco() {
		return std_endereco;
	}
	/**
	 * @param std_endereco the std_endereco to set
	 */
	public void setStd_endereco(String std_endereco) {
		this.std_endereco = std_endereco;
	}
	
}

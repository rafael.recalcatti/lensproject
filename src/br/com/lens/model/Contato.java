package br.com.lens.model;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Contato implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Integer id;
	
	@Column(length=1)
	private char tipo_contato;
	
	@Column(length=40)
	private String valor_contato;
	
	@Transient
	private String mask_contato;
	
	
	/**
	 * @return the mask_contato
	 */
	public String getMask_contato() {
		
		switch (this.tipo_contato) {
		case '0':
			this.mask_contato = "(99)999-999-999";
			break;

		case '1':
			this.mask_contato = "email";
			break;
			
		case '2':
			this.mask_contato = "(99)9999-9999";
			break;
			
		case '3':
			this.mask_contato = "(99)9999-9999";
			break;
			
		case '4':
			this.mask_contato = "+99 99 9999-9999";
			break;
			
		case '5':
			this.mask_contato = "{regex:'https://www.facebook.com/[a-z0-9\\u0600-\\u06FF]*'}";
			break;
			
		default:
			this.mask_contato = "(99)999-999-999";
			break;
		}
		
		return mask_contato;
	}

	/**
	 * @param mask_contato the mask_contato to set
	 */
	public void setMask_contato(String mask_contato) {
		this.mask_contato = mask_contato;
	}

	/**
	 * @return the icon_contato
	 */
	public String getIcon_contato() {
		
		switch (this.tipo_contato) {
		case '0':
			this.icon_contato = "fa-mobile-phone";
			break;

		case '1':
			this.icon_contato = "fa-envelope";
			break;
			
		case '2':
			this.icon_contato = "fa-phone-square";
			break;
			
		case '3':
			this.icon_contato = "fa-phone-square";
			break;
			
		case '4':
			this.icon_contato = "fa-whatsapp";
			break;
			
		case '5':
			this.icon_contato = "fa-facebook-official";
			break;
			
		default:
			this.icon_contato = "fa-mobile-phone";
			break;
		}
		
		return icon_contato;
	}

	/**
	 * @param icon_contato the icon_contato to set
	 */
	public void setIcon_contato(String icon_contato) {
		this.icon_contato = icon_contato;
	}

	@Transient
	private String icon_contato;
	
	
	@Transient
	private String label_contato;

	

	public String getLabel_Upper_contato() {
		return label_contato.toUpperCase();
	}
	
	/**
	 * @return the label_contato
	 */
	public String getLabel_contato() {
		
		switch (this.tipo_contato) {
		case '0':
			this.label_contato = "Telefone Celular";
			break;

		case '1':
			this.label_contato = "Email";
			break;
			
		case '2':
			this.label_contato = "Telefone Residencial";
			break;
			
		case '3':
			this.label_contato = "Telefone Comercial";
			break;
			
		case '4':
			this.label_contato = "WhatsApp";
			break;
			
		case '5':
			this.label_contato = "Facebook";
			break;
			
		default:
			this.label_contato = "Telefone Celular";
			break;
		}
		
		return label_contato;
	}

	/**
	 * @param label_contato the label_contato to set
	 */
	public void setLabel_contato(String label_contato) {
		this.label_contato = label_contato;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the tipo_contato
	 */
	public char getTipo_contato() {
		return tipo_contato;
	}

	/**
	 * @param tipo_contato the tipo_contato to set
	 */
	public void setTipo_contato(char tipo_contato) {
		this.tipo_contato = tipo_contato;
	}

	/**
	 * @return the valor_contato
	 */
	public String getValor_contato() {
		return valor_contato;
	}

	/**
	 * @param valor_contato the valor_contato to set
	 */
	public void setValor_contato(String valor_contato) {
		this.valor_contato = valor_contato;
	}

	
	
}

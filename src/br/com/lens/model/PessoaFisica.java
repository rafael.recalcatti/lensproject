package br.com.lens.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
//import org.apache.log4j.Logger;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
//import br.com.lens.bean.ContatoBean;

@Entity
public class PessoaFisica implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer id_pessoa_fisica;

	@Column(length = 50)
	private String nome;

	@Column(length = 50)
	private String nome_mae;

	@Column(length = 11)
	private String cpf;

	@Column(length = 12)
	private String registro_geral;

	@Column(length = 60)
	private String naturalidade;

	@Column(length = 15)
	private String std_civil;

	@Temporal(javax.persistence.TemporalType.DATE)
	private Date data_nascimento;

	@OneToMany(cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JoinColumn(name = "id_pessoa_fisica")
	private List<Endereco> enderecos = new ArrayList<Endereco>();

	@OneToMany(cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JoinColumn(name = "id_pessoa_fisica")
	private List<ContatoPessoa> contatos = new ArrayList<ContatoPessoa>();

	@OneToMany(cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	@JoinColumn(name = "id_pessoa_fisica")
	private List<ReferenciaPessoa> referencias = new ArrayList<ReferenciaPessoa>();

	/**
	 * @return the referencias
	 */
	public List<ReferenciaPessoa> getReferencias() {
		return referencias;
	}

	public PessoaFisica() {

	}

	/**
	 * @return the contatos
	 */
	public List<ContatoPessoa> getContatos() {
		return contatos;
	}

	/**
	 * @return the enderecos
	 */
	public List<Endereco> getEnderecos() {

		return enderecos;
	}

	/**
	 * @param enderecos the enderecos to set
	 */
	public void setEnderecos(List<Endereco> enderecos) {

		this.enderecos = enderecos;
	}

	public void removeEndereco(Endereco endereco) {
		this.enderecos.remove(endereco);
	}

	public void adicionaEndereco(Endereco endereco) {
		this.enderecos.add(endereco);
	}

	public void alterarEndereco(Endereco endereco) {
		if (endereco == null) {
			throw new IllegalArgumentException("Endere�o nao pode ser nulo");
		}
		int index = enderecos.indexOf(endereco);
		if (index > -1) {
			enderecos.set(index, endereco);
		}
	}

	public void removeContato(ContatoPessoa contatoPessoa) {
		this.contatos.remove(contatoPessoa);
	}

	public void adicionaContato(ContatoPessoa contatoPessoa) {
		this.contatos.add(contatoPessoa);
	}

	public void alterarContato(ContatoPessoa contatoPessoa) {
		if (contatoPessoa == null) {

			throw new IllegalArgumentException("Contato nao pode ser nulo");
		}
		int index = contatos.indexOf(contatoPessoa);
		if (index > -1) {
			contatos.set(index, contatoPessoa);
		}
	}

	public void removeReferencia(ReferenciaPessoa referenciaPessoa) {
		this.referencias.remove(referenciaPessoa);
	}

	public void adicionaReferencia(ReferenciaPessoa referenciaPessoa) {
		this.referencias.add(referenciaPessoa);
	}

	public void alterarReferencia(ReferenciaPessoa referenciaPessoa) {
		if (referenciaPessoa == null) {
			throw new IllegalArgumentException("Refer�ncia nao pode ser nulo");
		}
		int index = referencias.indexOf(referenciaPessoa);
		if (index > -1) {
			referencias.set(index, referenciaPessoa);
		}
	}

	/**
	 * @return the id_pessoa_fisica
	 */
	public Integer getId_pessoa_fisica() {
		return id_pessoa_fisica;
	}

	/**
	 * @param id_pessoa_fisica the id_pessoa_fisica to set
	 */
	public void setId_pessoa_fisica(Integer id_pessoa_fisica) {
		this.id_pessoa_fisica = id_pessoa_fisica;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the nome_mae
	 */
	public String getNome_mae() {
		return nome_mae;
	}

	/**
	 * @param nome_mae the nome_mae to set
	 */
	public void setNome_mae(String nome_mae) {
		this.nome_mae = nome_mae;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * @return the registro_geral
	 */
	public String getRegistro_geral() {
		return registro_geral;
	}

	/**
	 * @param registro_geral the registro_geral to set
	 */
	public void setRegistro_geral(String registro_geral) {
		this.registro_geral = registro_geral;
	}

	/**
	 * @return the naturalidade
	 */
	public String getNaturalidade() {
		return naturalidade;
	}

	/**
	 * @param naturalidade the naturalidade to set
	 */
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}

	/**
	 * @return the std_civil
	 */
	public String getStd_civil() {
		return std_civil;
	}

	/**
	 * @param std_civil the std_civil to set
	 */
	public void setStd_civil(String std_civil) {
		this.std_civil = std_civil;
	}

	/**
	 * @return the data_nascimento
	 */
	public Date getData_nascimento() {
		return data_nascimento;
	}

	/**
	 * @param data_nascimento the data_nascimento to set
	 */
	public void setData_nascimento(Date data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	/**
	 * @param contatos the contatos to set
	 */
	public void setContatos(List<ContatoPessoa> contatos) {
		this.contatos = contatos;
	}

	/**
	 * @param referencias the referencias to set
	 */
	public void setReferencias(List<ReferenciaPessoa> referencias) {
		this.referencias = referencias;
	}

}
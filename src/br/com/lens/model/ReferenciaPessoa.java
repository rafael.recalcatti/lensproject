package br.com.lens.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;                         

                                                                                    
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ReferenciaPessoa extends Contato {
	
	private static final long serialVersionUID = 1L;
	
	@Column(length=60)
	private String nome_referencia;
	
	@ManyToOne
	@JoinColumn(name = "id_pessoa_fisica", insertable = false, updatable = false)
	private PessoaFisica pessoaFisica;

	/**
	 * @return the nome_referencia
	 */
	public String getNome_referencia() {
		return nome_referencia;
	}

	/**
	 * @param nome_referencia the nome_referencia to set
	 */
	public void setNome_referencia(String nome_referencia) {
		this.nome_referencia = nome_referencia;
	}

	/**
	 * @return the pessoaFisica
	 */
	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	/**
	 * @param pessoaFisica the pessoaFisica to set
	 */
	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
	
	
}

package br.com.lens.util;

import java.text.ParseException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
//import javax.faces.convert.FacesConverter;
import javax.swing.text.MaskFormatter;

//@FacesConverter("estadoCivilConersor")
public class CpfConverter implements Converter {

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}

		String ret = null;

		try {

			MaskFormatter maskFormatter = new MaskFormatter("###.###.###-##");
			maskFormatter.setValueContainsLiteralCharacters(false);
			ret = maskFormatter.valueToString(value);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return ret;
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null) {
			return null;
		}

		Object ret = value.replace(".", "").replace("-", "");

		return ret;
	}
}

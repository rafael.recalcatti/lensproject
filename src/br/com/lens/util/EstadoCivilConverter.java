package br.com.lens.util;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
//import javax.faces.convert.FacesConverter;


@FacesConverter("estadoCivilConersor")
public class EstadoCivilConverter implements Converter {

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
			return null;
		}
		
		String[] std_civil = {"SOLTEIRO","CASADO","SEPARADO","VIUVO","UNI�O ESTAVEL"};
		System.out.println(">>>>>>LENDO<<<<<<");
		
		return std_civil[Integer.parseInt(value.toString())];
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null) {
			return null;
		}

		System.out.println(">>>>>>REGISTRANDO<<<<<<");
		return value;
	}
}


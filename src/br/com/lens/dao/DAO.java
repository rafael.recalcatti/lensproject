package br.com.lens.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
//import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
//import br.com.lens.bean.EnderecoBean;
import br.com.lens.model.Endereco;
import br.com.lens.model.PessoaFisica;

public class DAO<T> {

	private final Class<T> classe;

	public DAO(Class<T> classe) {
		this.classe = classe;
	}

	public void adiciona(T t) {

		// consegue a entity manager
		EntityManager em = new JPAUtil().getEntityManager();
		// abre transacao
		em.getTransaction().begin();
		// persiste o objeto
		em.persist(t);
		// commita a transacao
		em.getTransaction().commit();
		// fecha a entity manager
		em.close();
	}

	public void atualiza(T t) {
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		em.merge(t);

		em.getTransaction().commit();
		em.close();
	}

	public void remove(T t) {

		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();

		em.remove(em.merge(t));

		em.getTransaction().commit();
		em.close();
	}

	public T buscaPorId(Integer id) {

		T instancia = null;

		try {

			EntityManager em = new JPAUtil().getEntityManager();
			instancia = em.find(classe, id);
			em.close();

		} catch (Exception e) {
			System.out.println(">>>>>>>>>>>>>>>ERRO:::" + e);
			return instancia;
		}

		return instancia;
	}

	public List<T> listaTodos() {

		try {
			
			EntityManager em = new JPAUtil().getEntityManager();
			CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
			query.select(query.from(classe));

			List<T> lista = em.createQuery(query).getResultList();
			em.close();
			return lista;
			
		} catch (Exception e) {
			return null;
		}
		
	}

	public List<PessoaFisica> listaTodos(String arg, boolean cityFilter) {

		EntityManager em = new JPAUtil().getEntityManager();
		List<PessoaFisica> res = new ArrayList<PessoaFisica>();
		List<PessoaFisica> res_outher = new ArrayList<PessoaFisica>();

		try {

			if (cityFilter) {

				TypedQuery<PessoaFisica> query = em.createQuery("SELECT p FROM PessoaFisica p", PessoaFisica.class);
				res = query.getResultList();
				res_outher = new ArrayList<PessoaFisica>();

				for (PessoaFisica pessoaFisica : res) {

					for (Endereco endereco : pessoaFisica.getEnderecos()) {

						arg = arg.toUpperCase().trim();
						String address = endereco.getLogradouro() + endereco.getEndereco() + ","
								+ endereco.getNumero();

						if (endereco.getCidade().toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (endereco.getBairro().toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (endereco.getEndereco().toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (endereco.getLogradouro().toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (String.valueOf(endereco.getNumero()).contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (endereco.getCep().toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (endereco.getUf().toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						} else if (address.toUpperCase().contains(arg)) {
							res_outher.add(pessoaFisica);
						}

					}
				}

				return res_outher;

			} else {

				TypedQuery<PessoaFisica> query = em
						.createQuery(
								"SELECT p FROM PessoaFisica p " + "WHERE p.nome LIKE :arg "
										+ "OR p.nome_mae LIKE :arg " + "OR p.cpf LIKE :arg "
										+ "OR p.registro_geral LIKE :arg " + "GROUP BY p.id_pessoa_fisica",
								PessoaFisica.class);

				res = query.setParameter("arg", "%" + arg + "%").getResultList();

				return res;
			}

		} catch (Exception e) {

			return res;
		}
	}

	public int contaTodos() {

		EntityManager em = new JPAUtil().getEntityManager();

		long result = (Long) em.createQuery("select count(n) from pessoafisica n").getSingleResult();
		em.close();

		return (int) result;
	}

	public List<T> listaTodosPaginada(int firstResult, int maxResults) {
		EntityManager em = new JPAUtil().getEntityManager();
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));

		List<T> lista = em.createQuery(query).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();

		em.close();
		return lista;
	}

}
